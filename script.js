let buttons = document.querySelectorAll('.btn');

buttons.forEach((item) => {
item.dataset.name = item.textContent.toLowerCase();
});

let prevButton;

document.addEventListener('keydown', (event) => {
    let currentButton = document.querySelector('.btn[data-name=' + event.key.toLowerCase() + ']');
    if (prevButton && (prevButton !== currentButton)) prevButton.style.backgroundColor = '#33333a'; 
    
    if (currentButton) {
        if (currentButton.style.backgroundColor != 'blue') currentButton.style.backgroundColor = 'blue';
    }
    prevButton = currentButton;
})
